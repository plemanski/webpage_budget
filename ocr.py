#!/usr/bin/env python

import pytesseract
import requests
import sys

from PIL import Image
from PIL import ImageFilter
from StringIO import StringIO


def process_image(path):
	image = Image.open(path)
	image.filter(ImageFilter.SHARPEN)
	return pytesseract.image_to_string(image)

with open(sys.argv[2], 'w') as f:
	f.write(process_image(sys.argv[1]))
