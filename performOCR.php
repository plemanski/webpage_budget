<?php
#-------------------------------------------------------------------------------
# EE368 Digital Image Processing
# Android Tutorial #3: Server-Client Interaction Example for Image Processing
# Author: Derek Pang (dcypang@stanford.edu), David Chen (dmchen@stanford.edu)
#------------------------------------------------------------------------------

#function for streaming file to client
function streamFile($location, $filename, $mimeType='application/octet-stream')
{ if(!file_exists($location))
  { header ("HTTP/1.0 404 Not Found");
    return;
  }
  
  $size=filesize($location);
  $time=date('r',filemtime($location));
  #html response header
  header('Content-Description: File Transfer');	
  header("Content-Type: $mimeType"); 
  header('Cache-Control: public, must-revalidate, max-age=0');
  header('Pragma: no-cache');  
  header('Accept-Ranges: bytes');
  header('Content-Length:'.($size));
  header("Content-Disposition: inline; filename=$filename");
  header("Content-Transfer-Encoding: binary\n");
  header("Last-Modified: $time");
  header('Connection: close');      

  ob_clean();
  flush();
  readfile($location);
	
}
 $dir = 'wonsz_upload';

 // create new directory with 744 permissions if it does not exist yet
 // owner will be the user/group the PHP script is run under
 if ( !file_exists($dir) ) {
  mkdir ($dir, 0777);
 }


#**********************************************************
#Main script
#**********************************************************

#<1>set target path for storing photo uploads on the server
$photo_upload_path = "upload/";
$photo_upload_path = $photo_upload_path. basename( $_FILES['uploadedfile']['name']); 
#<2>set target path for storing result on the server
$processed_photo_output_path = "$dir/processed_";
$processed_photo_output_path = $processed_photo_output_path. basename( $_FILES['uploadedfile']['name']) . '.txt'; 
$downloadFileName = 'processed_' . basename( $_FILES['uploadedfile']['name']) . '.txt'; 

#<3>modify maximum allowable file size to 10MB and timeout to 300s
ini_set('upload_max_filesize', '21M');  
ini_set('post_max_size', '30M');  
ini_set('max_input_time', 3000);  
ini_set('max_execution_time', 3000);  

#<4>Get and stored uploaded photos on the server
if(copy($_FILES['uploadedfile']['tmp_name'], $photo_upload_path)) {
	
	#<5> execute matlab image processing algorithm
	#example: Compute and display SIFT features using VLFeat and Matlabo
  echo "$photo_upload_path<br>";
  echo "$processed_photo_output_path<br>";
  $command = '/usr/bin/python /var/www/html/ocr.py /var/www/html/' . $photo_upload_path . ' /var/www/html/' . $processed_photo_output_path;
  echo $command;
  exec($command, $output);
  var_dump($my_output);
  echo "<br>wonsz!";
	#<6>stream processed photo to the client
	streamFile($processed_photo_output_path, $downloadFileName, "application/octet-stream");
} else{
    echo "There was an error uploading the file to $photo_upload_path !";
    echo "".$_FILES['uploadedfile']['error'];
}
 
?>



